import psycopg2
import json

try:
    conn = psycopg2.connect(
            host="localhost",
            database="postgres",
            user="postgres",
            password="postgres",
            port="5432"
        )
    cur = conn.cursor()
    ur_dict = {
        "sbis": [
            {
                "sbi_id": "sbi-mvp01-20200325-00002",
                "mccs_allocation": {
                    "subarray_beam_ids": [
                        "beam A"
                    ],
                    "station_ids": [
                        [
                            1,
                            2
                        ]
                    ],
                    "channel_blocks": [
                        3
                    ]
                },
                "field_configurations": {
                    "target field": {
                        "targets": {
                            "target #1": {
                                "kind": "driftscan",
                                "target_name": "Drift scan at 45 degs",
                                "reference_frame": "HORIZON",
                                "az": 180.0,
                                "el": 45.0
                            }
                        }
                    }
                },
                "subarray_beam_configurations": [
                    {
                        "subarray_beam_configuration_id": "beam A config 1",
                        "subarray_beam_id": "beam A",
                        "update_rate": 0.0,
                        "antenna_weights": [
                            1.0,
                            1.0,
                            1.0
                        ],
                        "phase_centre": [
                            0.0,
                            0.0
                        ],
                        "channels": [
                            [
                                0,
                                8,
                                1,
                                1
                            ],
                            [
                                8,
                                8,
                                2,
                                1
                            ],
                            [
                                24,
                                16,
                                2,
                                1
                            ]
                        ]
                    }
                ],
                "target_beam_configurations": [
                    {
                        "target_beam_id": "target #1 with beam A config 1",
                        "target": "target #1",
                        "subarray_beam_configuration": "beam A config 1"
                    }
                ],
                "scan_definitions": [
                    {
                        "scan_definition_id": "drift scan",
                        "scan_duration": 12800,
                        "target_beam_configurations": [
                            "target #1 with beam A config 1"
                        ]
                    }
                ],
                "scan_sequence": [
                    "drift scan",
                    "drift scan"
                ]
            },
            {
                "sbi_id": "sbi-mvp01-20200325-99998",
                "mccs_allocation": {
                    "subarray_beam_ids": [
                        "beam A"
                    ],
                    "station_ids": [
                        [
                            1,
                            2
                        ]
                    ],
                    "channel_blocks": [
                        3
                    ]
                },
                "field_configurations": {
                    "target field": {
                        "targets": {
                            "target #1": {
                                "kind": "driftscan",
                                "target_name": "Drift scan at 45 degs",
                                "reference_frame": "HORIZON",
                                "az": 180.0,
                                "el": 45.0
                            }
                        }
                    }
                },
                "subarray_beam_configurations": [
                    {
                        "subarray_beam_configuration_id": "beam A config 1",
                        "subarray_beam_id": "beam A",
                        "update_rate": 0.0,
                        "antenna_weights": [
                            1.0,
                            1.0,
                            1.0
                        ],
                        "phase_centre": [
                            0.0,
                            0.0
                        ],
                        "channels": [
                            [
                                0,
                                8,
                                1,
                                1
                            ],
                            [
                                8,
                                8,
                                2,
                                1
                            ],
                            [
                                24,
                                16,
                                2,
                                1
                            ]
                        ]
                    }
                ],
                "target_beam_configurations": [
                    {
                        "target_beam_id": "target #1 with beam A config 1",
                        "target": "target #1",
                        "subarray_beam_configuration": "beam A config 1"
                    }
                ],
                "scan_definitions": [
                    {
                        "scan_definition_id": "drift scan",
                        "scan_duration": 12800,
                        "target_beam_configurations": [
                            "target #1 with beam A config 1"
                        ]
                    }
                ],
                "scan_sequence": [
                    "drift scan",
                    "drift scan"
                ]
            }
        ]
    }

    # create t_json table with JSON datatype
    cur.execute("CREATE TABLE t_json (info jsonb NOT NULL)")

    # insert JSON data
    cur.execute("""INSERT INTO t_json (  info ) VALUES ( '{0}' ) """.format(json.dumps(ur_dict)))

    cur.close()

    conn.commit()
    conn.close()

except (Exception,psycopg2.Error) as error:
    print(error)