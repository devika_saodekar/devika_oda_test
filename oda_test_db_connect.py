import psycopg2

try:
    # connect to the database
    con = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="postgres",
        port="5432"
    )

    # cursor
    cur = con.cursor()

    # create orders table
    cur.execute("CREATE TABLE orders (id serial NOT NULL PRIMARY KEY,info jsonb NOT NULL)")
    # insert records into table
    cur.execute("""insert into orders (id,info) values (10,'{"customer": "Lily Bush", "items": {"product": "Diaper","qty": 24}}')""")
    cur.execute("""insert into orders (id,info) values (20,'{"customer": "Josh William", "items": {"product": "Toy Car","qty": 1}}')""")
    cur.execute("""insert into orders (id,info) values (30,'{"customer": "Mary Clark", "items": {"product": "Toy Train","qty": 2}}')""")
    # execute the query
    cur.execute("select * from orders")

    #update a table row
    cur.execute("""UPDATE orders SET info = jsonb_set(info, '{qty}', '101', true) WHERE info ->> 'customer' = 'Lily Bush'""")


    # Fetch the rows
    rows = cur.fetchall()

    for r in rows:
        print(f"id {r[0]} info {r[1]}")

    # commit the transaction#
    con.commit()

    # close the cursor
    cur.close()

    # close connection
    con.close()

except (Exception, psycopg2.Error) as error:
    print(error)