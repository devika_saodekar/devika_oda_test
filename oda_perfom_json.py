import os
import json
import psycopg2

try:
    with psycopg2.connect(host="localhost",database="postgres",user="postgres",password="postgres",port="5432") as conn:
        with conn.cursor() as cur:
        # create t_json table with JSON datatype
        # cur.execute("CREATE TABLE t_json (info jsonb NOT NULL)")
            # you can specify a directory path where all the JSON files exist
            directory = '/home/buttons/Devika/ODA_Test_Data'
            for filename in os.listdir(directory):
                if filename.endswith(".json"): #searching files with .JSON extension
                    print(os.path.join(directory, filename)) #printing JSON File Path
                    file_path = os.path.join(directory, filename)
                    with open(file_path) as my_file: # reading files content from directory
                        data = json.load(my_file)
                        # insert JSON data
                        cur.execute("""INSERT INTO json_perf_table (  info ) VALUES ( '{0}' ) """.format(json.dumps(data)))

                        conn.commit()
                else:
                    continue

except Exception as error:
    print(error)
