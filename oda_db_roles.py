import psycopg2
from psycopg2 import sql

try:
    def prompt_username(con):
        cur = con.cursor()
        while True:
            username = input("Enter User Name : ")
            cur.execute("SELECT COUNT(*) FROM pg_catalog.pg_roles WHERE rolname = %s", [username])
            n, = cur.fetchone()
            if n == 0:
                return username
            print("User already exists.")


    def userCreation():
        con = psycopg2.connect(
            host="localhost",
            database="postgres",
            user="postgres",
            password="postgres",
            port="5432"
        )

        username = prompt_username(con)
        password = input(f"Enter Password for {username} : ")
        query = sql.SQL("CREATE ROLE {0} LOGIN PASSWORD {1}").format(
            sql.Identifier(username),
            sql.Literal(password),
        )
        cur = con.cursor()
        cur.execute(query.as_string(con))
        cur.execute("COMMIT")


    if __name__ == '__main__':
        userCreation()

except (Exception, psycopg2.Error) as error:
    print(error)