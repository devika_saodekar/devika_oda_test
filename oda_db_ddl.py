import psycopg2

try:
    # connect to the database
    con = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="postgres",
        port="5432"
    )

    # cursor
    cur = con.cursor()

    # create a placeholder table for SBDs
    cur.execute("CREATE TABLE tab_oda_sbd (id serial NOT NULL PRIMARY KEY,info jsonb NOT NULL,sbd_id text NOT NULL,version serial NOT NULL, created_by text NOT NULL,created_on timestamp NOT NULL, last_modified_on timestamp NOT NULL,last_modified_by text NOT NULL )")

    # create a placeholder table for Projects
    cur.execute("CREATE TABLE tab_oda_prj (id serial NOT NULL PRIMARY KEY,info jsonb NOT NULL,prj_id text NOT NULL,version serial NOT NULL, created_by text NOT NULL,created_on timestamp NOT NULL, last_modified_on timestamp NOT NULL,last_modified_by text NOT NULL )")

    # create a placeholder table for Observing Programmes
    cur.execute("CREATE TABLE tab_oda_obs_prj (id serial NOT NULL PRIMARY KEY,info jsonb NOT NULL,obs_prj_id text NOT NULL,version serial NOT NULL, created_by text NOT NULL,created_on timestamp NOT NULL, last_modified_on timestamp NOT NULL,last_modified_by text NOT NULL )")

    # create a placeholder table for SBIs
    cur.execute("CREATE TABLE tab_oda_sbi (id serial NOT NULL PRIMARY KEY,info jsonb NOT NULL,sbi_id text NOT NULL,version integer default 1, created_by text NOT NULL,created_on timestamp NOT NULL, last_modified_on timestamp NOT NULL,last_modified_by text NOT NULL )")

    # create a placeholder table for Execution Blocks
    cur.execute("CREATE TABLE tab_oda_exe_blk (id serial NOT NULL PRIMARY KEY,info jsonb NOT NULL,exe_blk_id text NOT NULL,version integer default 1, created_by text NOT NULL,created_on timestamp NOT NULL, last_modified_on timestamp NOT NULL,last_modified_by text NOT NULL )")

    # commit the transaction#
    con.commit()

    # close the cursor
    cur.close()

    # close connection
    con.close()

except (Exception, psycopg2.Error) as error:
    print(error)