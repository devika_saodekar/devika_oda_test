import json
import psycopg2

try:
    with psycopg2.connect(host="localhost",database="postgres",user="postgres",password="postgres",port="5432") as conn:
        with conn.cursor() as cur:
            with open('/home/buttons/Devika/testfile_sbis_unique.json') as my_file:
                data = json.load(my_file)
                print(type(data))
                # create t_json table with JSON datatype
                cur.execute("CREATE TABLE json_table (info jsonb NOT NULL)")

                # insert JSON data
                cur.execute("""INSERT INTO json_table (  info ) VALUES ( '{0}' ) """.format(json.dumps(data)))

                conn.commit()

except Exception as error:
    print(error)