import os
import json
import psycopg2

try:
    with psycopg2.connect(host="localhost",database="postgres",user="postgres",password="postgres",port="5432") as conn:
        with conn.cursor() as cur:
            #create tab_oda_sbd table with JSON datatype
            cur.execute("CREATE TABLE tab_oda_sbd (id serial NOT NULL PRIMARY KEY,info jsonb NOT NULL,sbd_id text NOT NULL,version text NOT NULL, created_by text NOT NULL,created_on text NOT NULL, last_modified_on text NOT NULL,last_modified_by text NOT NULL )")
            cur.execute("CREATE TABLE tab_oda_sbd_1 (id serial NOT NULL PRIMARY KEY,info jsonb NOT NULL)")
            # you can specify a directory path where all the JSON files exist
            directory = '/home/buttons/Devika/Test'
            for filename in os.listdir(directory):
                if filename.endswith(".json"): #searching files with .JSON extension
                    print(os.path.join(directory, filename)) #printing JSON File Path
                    file_path = os.path.join(directory, filename)
                    with open(file_path) as my_file: # reading files content from directory
                        data = json.load(my_file)
                        # insert JSON data in tab_oda_sbd_1 table
                        cur.execute("""INSERT INTO tab_oda_sbd_1 (  info ) VALUES ( '{0}' ) """.format(json.dumps(data)))
                        # insert JSON data in tab_oda_sbd_1 table
                        cur.execute("""INSERT INTO tab_oda_sbd ( info,sbd_id,version,created_by,created_on,last_modified_on,last_modified_by) select info,info ->>'sbd_id',info ->'metadata' ->> 'version',info ->'metadata' ->> 'created_by' ,info ->'metadata' ->> 'created_on',info ->'metadata' ->> 'last_modified_on',info ->'metadata' ->> 'last_modified_by' from tab_oda_sbd_1""")
                        conn.commit()
                else:
                    continue

except Exception as error:
    print(error)
